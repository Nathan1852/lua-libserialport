local ffi = require("ffi")

ffi.cdef[[
	typedef struct sp_port sp_port;
	typedef struct sp_port_config sp_port_config;
	typedef struct sp_event_set sp_event_set;

	/** sp_event_set */
	struct sp_event_set {
	    /** Array of OS-specific handles. */
	    void *handles;
	    /** Array of bitmasks indicating which events apply for each handle. */
	    enum sp_event *masks;
	    /** Number of handles. */
	    unsigned int count;
	};

	/** Return values. */
 	enum sp_return {
    	/** Operation completed successfully. */
  		SP_OK = 0,
  		/** Invalid arguments were passed to the function. */
		SP_ERR_ARG = -1,
		/** A system error occurred while executing the operation. */
		SP_ERR_FAIL = -2,
		/** A memory allocation failed while executing the operation. */
		SP_ERR_MEM = -3,
		/** The requested operation is not supported by this system or device. */
		SP_ERR_SUPP = -4
	};

 	/** Port access modes. */
	enum sp_mode {
	    /** Open port for read access. */
	    SP_MODE_READ = 1,
	    /** Open port for write access. */
	    SP_MODE_WRITE = 2,
	    /** Open port for read and write access. @since 0.1.1 */
	    SP_MODE_READ_WRITE = 3
	};

	/** Port events. */
	enum sp_event {
	    /** Data received and ready to read. */
	    SP_EVENT_RX_READY = 1,
	    /** Ready to transmit new data. */
	    SP_EVENT_TX_READY = 2,
	    /** Error occurred. */
	    SP_EVENT_ERROR = 4
	};

	/** Buffer selection. */
	enum sp_buffer {
	    /** Input buffer. */
	    SP_BUF_INPUT = 1,
	    /** Output buffer. */
	    SP_BUF_OUTPUT = 2,
	    /** Both buffers. */
	    SP_BUF_BOTH = 3
	};

	/** Parity settings. */
	enum sp_parity {
	    /** Special value to indicate setting should be left alone. */
	    SP_PARITY_INVALID = -1,
	    /** No parity. */
	    SP_PARITY_NONE = 0,
	    /** Odd parity. */
	    SP_PARITY_ODD = 1,
	    /** Even parity. */
	    SP_PARITY_EVEN = 2,
	    /** Mark parity. */
	    SP_PARITY_MARK = 3,
	    /** Space parity. */
	    SP_PARITY_SPACE = 4
	};

	/** RTS pin behaviour. */
	enum sp_rts {
	    /** Special value to indicate setting should be left alone. */
	    SP_RTS_INVALID = -1,
	    /** RTS off. */
	    SP_RTS_OFF = 0,
	    /** RTS on. */
	    SP_RTS_ON = 1,
	    /** RTS used for flow control. */
	    SP_RTS_FLOW_CONTROL = 2
	};

	/** CTS pin behaviour. */
	enum sp_cts {
	    /** Special value to indicate setting should be left alone. */
	    SP_CTS_INVALID = -1,
	    /** CTS ignored. */
	    SP_CTS_IGNORE = 0,
	    /** CTS used for flow control. */
	    SP_CTS_FLOW_CONTROL = 1
	};

	/** DTR pin behaviour. */
	enum sp_dtr {
	    /** Special value to indicate setting should be left alone. */
	    SP_DTR_INVALID = -1,
	    /** DTR off. */
	    SP_DTR_OFF = 0,
	    /** DTR on. */
	    SP_DTR_ON = 1,
	    /** DTR used for flow control. */
	    SP_DTR_FLOW_CONTROL = 2
	};

	/** DSR pin behaviour. */
	enum sp_dsr {
	    /** Special value to indicate setting should be left alone. */
	    SP_DSR_INVALID = -1,
	    /** DSR ignored. */
	    SP_DSR_IGNORE = 0,
	    /** DSR used for flow control. */
	    SP_DSR_FLOW_CONTROL = 1
	};

	/** XON/XOFF flow control behaviour. */
	enum sp_xonxoff {
	    /** Special value to indicate setting should be left alone. */
	    SP_XONXOFF_INVALID = -1,
	    /** XON/XOFF disabled. */
	    SP_XONXOFF_DISABLED = 0,
	    /** XON/XOFF enabled for input only. */
	    SP_XONXOFF_IN = 1,
	    /** XON/XOFF enabled for output only. */
	    SP_XONXOFF_OUT = 2,
	    /** XON/XOFF enabled for input and output. */
	    SP_XONXOFF_INOUT = 3
	};

	/** Standard flow control combinations. */
	enum sp_flowcontrol {
	    /** No flow control. */
	    SP_FLOWCONTROL_NONE = 0,
	    /** Software flow control using XON/XOFF characters. */
	    SP_FLOWCONTROL_XONXOFF = 1,
	    /** Hardware flow control using RTS/CTS signals. */
	    SP_FLOWCONTROL_RTSCTS = 2,
	    /** Hardware flow control using DTR/DSR signals. */
	    SP_FLOWCONTROL_DTRDSR = 3
	};

	/** Input signals. */
	enum sp_signal {
	    /** Clear to send. */
	    SP_SIG_CTS = 1,
	    /** Data set ready. */
	    SP_SIG_DSR = 2,
	    /** Data carrier detect. */
	    SP_SIG_DCD = 4,
	    /** Ring indicator. */
	    SP_SIG_RI = 8
	};

	/** Transport types. */
	enum sp_transport {
	    /** Native platform serial port. @since 0.1.1 */
	    SP_TRANSPORT_NATIVE,
	    /** USB serial port adapter. @since 0.1.1 */
	    SP_TRANSPORT_USB,
	    /** Bluetooth serial port adapter. @since 0.1.1 */
	    SP_TRANSPORT_BLUETOOTH
	};

	/* Port enumeration */
	enum sp_return sp_get_port_by_name ( const char *portname, struct sp_port **port_ptr );
	void sp_free_port ( struct sp_port *port );
	enum sp_return sp_list_ports ( struct sp_port ***list_ptr );
	enum sp_return sp_copy_port ( const struct sp_port *port, struct sp_port **copy_ptr );
	void sp_free_port_list ( struct sp_port **ports );

	/* Port handling */
	enum sp_return sp_open ( struct sp_port* port, enum sp_mode flags );
	enum sp_return sp_close ( struct sp_port* port );
	char* sp_get_port_name (	const struct sp_port *port );
	char* sp_get_port_description ( const struct sp_port *port );
	enum sp_transport sp_get_port_transport ( const struct sp_port *port );
	enum sp_return sp_get_port_usb_bus_address ( const struct sp_port *port, int *usb_bus, int *usb_address );
	enum sp_return sp_get_port_usb_vid_pid ( const struct sp_port *port, int *usb_viid, int *usb_pid );
	char* sp_get_port_usb_manufacturer ( const struct sp_port *port );
	char* sp_get_port_usb_product ( const struct sp_port *port );
	char* sp_get_port_usb_serial ( const struct sp_port *port );
	char* sp_get_port_bluetooth_address ( const struct sp_port *port );
	enum sp_return sp_get_port_handle ( const struct sp_port *port, void *result_ptr );

	/* Configuration */
	enum sp_return sp_new_config ( struct sp_port_config **config_ptr );
	void sp_free_config ( struct sp_port_config *config );
	enum sp_return sp_get_config ( struct sp_port *port, struct sp_port_config *config );
	enum sp_return sp_set_config ( struct sp_port *port, const struct sp_port_config *config );
	enum sp_return sp_set_baudrate ( struct sp_port *port, int baudrate );
	enum sp_return sp_get_config_baudrate ( const struct sp_port_config *config, int *baudrate_ptr );
	enum sp_return sp_set_config_baudrate ( struct sp_port_config *config, int baudrate );
	enum sp_return sp_set_bits ( struct sp_port *port, int bits );
	enum sp_return sp_get_config_bits ( const struct sp_port_config *config, int *bits_ptr );
	enum sp_return sp_set_config_bits ( struct sp_port_config *config, int bits );
	enum sp_return sp_set_parity ( struct sp_port *port, enum sp_parity parity );
	enum sp_return sp_get_config_parity ( const struct sp_port_config *config, enum sp_parity *parity_ptr );
	enum sp_return sp_set_config_parity ( struct sp_port_config *config, enum sp_parity parity );
	enum sp_return sp_set_stopbits ( struct sp_port *port, int stopbits );
	enum sp_return sp_get_config_stopbits ( const struct sp_port_config *config, int *stopbits_ptr );
	enum sp_return sp_set_config_stopbits ( struct sp_port_config *config, int stopbits );
	enum sp_return sp_set_rts (struct sp_port *port, enum sp_rts rts );
	enum sp_return sp_get_config_rts ( const struct sp_port_config *config, enum sp_rts *rts_ptr );
	enum sp_return sp_set_config_rts ( struct sp_port_config *config, enum sp_rts rts );
	enum sp_return sp_set_cts ( struct sp_port *port, enum sp_cts cts );
	enum sp_return sp_get_config_cts ( const struct sp_port_config *config, enum sp_cts *cts_ptr );
	enum sp_return sp_set_config_cts ( struct sp_port_config *config, enum sp_cts cts );
	enum sp_return sp_set_dtr ( struct sp_port *port, enum sp_dtr dtr );
	enum sp_return sp_get_config_dtr ( const struct sp_port_config *config, enum sp_dtr *dtr_ptr );
	enum sp_return sp_set_config_dtr ( struct sp_port_config *config, enum sp_dtr dtr );
	enum sp_return sp_set_dsr ( struct sp_port *port, enum sp_dsr dsr );
	enum sp_return sp_get_config_dsr ( const struct sp_port_config *config, enum sp_dsr *dsr_ptr );
	enum sp_return sp_set_config_dsr ( struct sp_port_config *config, enum sp_dsr dsr );
	enum sp_return sp_set_xon_xoff ( struct sp_port *port, enum sp_xonxoff xon_xoff );
	enum sp_return sp_get_config_xon_xoff ( const struct sp_port_config *config, enum sp_xonxoff *xon_xoff_ptr );
	enum sp_return sp_set_config_xon_xoff ( struct sp_port_config *config, enum sp_xonxoff xon_xoff );
	enum sp_return sp_set_config_flowcontrol ( struct sp_port_config *config, enum sp_flowcontrol flowcontrol );
	enum sp_return sp_set_flowcontrol ( struct sp_port *port, enum sp_flowcontrol flowcontrol );

	/* Data handling */
	enum sp_return sp_blocking_read ( struct sp_port *port, void *buf, size_t count, unsigned int timeout_ms );
	enum sp_return sp_blocking_read_next ( struct sp_port *port, void *buf, size_t count, unsigned int timeout_ms );
	enum sp_return sp_nonblocking_read ( struct sp_port *port, void *buf, size_t count );
	enum sp_return sp_blocking_write ( struct sp_port *port, const void *buf, size_t count, unsigned int timeout_ms );
	enum sp_return sp_nonblocking_write ( struct sp_port *port, const void *buf, size_t count );
	enum sp_return sp_input_waiting ( struct sp_port *port );
	enum sp_return sp_output_waiting ( struct sp_port *port );
	enum sp_return sp_flush ( struct sp_port *port, enum sp_buffer buffers );
	enum sp_return sp_drain ( struct sp_port *port );

	/* Waiting */
	enum sp_return sp_new_event_set ( struct sp_event_set **result_ptr );
	enum sp_return sp_add_port_events ( struct sp_event_set *event_set, const struct sp_port *port, enum sp_event mask );
	enum sp_return sp_wait ( struct sp_event_set *event_set, unsigned int timeout_ms );
	void sp_free_event_set ( struct sp_event_set *event_set );

	/* Signals */
	enum sp_return sp_get_signals ( struct sp_port *port, enum sp_signal *signal_mask );
	enum sp_return sp_start_break ( struct sp_port *port );
	enum sp_return sp_end_break ( struct sp_port *port );

	/* Errors */
	int sp_last_error_code ( );
	char* sp_last_error_message ( );
	void sp_free_error_message ( char *message );

	/* Versions */
	int sp_get_major_package_version ( );
	int sp_get_minor_package_version ( );
	int sp_get_micro_package_version ( );;
	const char* sp_get_package_version_string ( );;;
	int sp_get_current_lib_version ( );
	int sp_get_revision_lib_version ( );
	int sp_get_age_lib_version ( );
	const char* sp_get_lib_version_string ( );


	typedef struct port{
		sp_port * __port[1];
	} port;

	typedef struct ports{
		sp_port ** __ports[1];
	} ports;

	typedef struct config{
		sp_port_config * __config[1];
	} config;

	typedef struct events{
		sp_event_set * __events[1];
	} events;

]]

local bit = require("bit")

local ser = {}

function ser.init( path )
	local path = path or "libserialport"

	local success, lib = pcall( ffi.load, path )
	if success then
		ser.__lib = lib
		ser.__port = ffi.metatype( ffi.typeof( "port" ), ser.__port_meta )
		ser.__ports = ffi.metatype( ffi.typeof( "ports" ), ser.__ports_meta )
		ser.__config = ffi.metatype( ffi.typeof( "config" ), ser.__config_meta )
		ser.__events = ffi.metatype( ffi.typeof( "events" ), ser.__config_events )
		ser.__uint8 = ffi.typeof("uint8_t[?]")
		return true
	else
		return false, lib
	end
end

function ser.getPort( name )

	assert( type(name) == "string", "Name must be a string")

	local port = ser.__port()

	local res = ser.__lib.sp_get_port_by_name( name, port.__port )

	if res == ser.__lib.SP_OK then
		return port
	else
		return ser.__getLastErrorMesage( res )
	end
end

function ser.__freePort( port )

	assert( ffi.istype(ser.__port, port), "Port must be a valid port" )

	ser.__lib.sp_free_port( port.__port[0] )

end

function ser.listPorts()

	local lib = ser.__lib
	local ports = ser.__ports()

	local res = lib.sp_list_ports( ports.__ports )

	if res == lib.SP_OK then
		return ports
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.getListPortCount( ports )

	assert( ffi.istype(ser.__ports, ports), "Ports must be a valid port list" )

	local num = 0
	while ports.__ports[0][num] ~= nil do
		num = num + 1
	end

	return num
end

function ser.getListNames( ports )

	assert( ffi.istype(ser.__ports, ports), "Ports must be a valid port list" )

	local num = ports:getPortCount( ports )

	local names = {}

	for i = 0, num-1 do
		local name = ser.__lib.sp_get_port_name( ports.__ports[0][i] )
		if name ~= nil then
			names[i + 1] = ffi.string( name )
		else
			names[i + 1] = ""
		end
	end

	return names
end

function ser.getListDescriptions( ports )

	assert( ffi.istype(ser.__ports, ports), "Ports must be a valid port list" )

	local num = ports:getPortCount( ports )

	local descs = {}

	for i = 0, num-1 do
		local desc = ser.__lib.sp_get_port_description( ports.__ports[0][i] )
		if desc ~= nil then
			descs[i + 1] = ffi.string( desc )
		else
			descs[i + 1] = ""
		end
	end

	return descs

end

function ser.getListTransportTypes( ports )

	assert( ffi.istype(ser.__ports, ports), "Ports must be a valid port list" )

	local num = ports:getPortCount( ports )

	local types = {}

	for i = 0, num-1 do
		local typ = ser.__lib.sp_get_port_transport( ports.__ports[0][i] )
		if typ ~= nil then
			if typ == ser.__lib.SP_TRANSPORT_NATIVE then
				types[i + 1] = "Native platform serial port"
			elseif typ == ser.__lib.SP_TRANSPORT_USB then
				types[i + 1] = "USB serial port adapter"
			elseif typ == ser.__lib.SP_TRANSPORT_BLUETOOTH then
				types[i + 1] = "Bluetooth serial port adapter"
			end
		else
			types[i + 1] = ""
		end
	end

	return types
end

function ser.__freePortList( ports )

	assert( ffi.istype(ser.__ports, ports), "Ports must be a valid port list" )

	ser.__lib.sp_free_port_list( ports.__ports[0] )

end

function ser.getPortFromList( ports, index )

	assert( ffi.istype(ser.__ports, ports), "Ports must be a valid port list" )
	assert( type(index) == "number", "Port index must be a positive integer" )

	local num = ports:getPortCount( ports )

	if index > 0 and index < num + 1 then

		local newPort = ser.__port()

		local res = ser.__lib.sp_copy_port( ports.__ports[0][index - 1], newPort.__port )
		if res == ser.__lib.SP_OK then
			return newPort
		else
			return ser.__getLastErrorMesage( res )
		end

	end
end

function ser.copyPort( port )

	assert( ffi.istype(ser.__port, port), "Port must be a valid port" )

	local newPort = ser.__port()

	local res = ser.__lib.sp_copy_port( port.__port[0], newPort.__port )
	if res == ser.__lib.SP_OK then
		return newPort
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.open( port, mode )

	assert( ffi.istype(ser.__port, port), "Port must be a valid port" )

	local flag;
	if mode == "r" or mode == "read" then
		flag = ser.__lib.SP_MODE_READ
	elseif mode == "w" or mode == "write" then
		flag = ser.__lib.SP_MODE_WRITE
	elseif mode == "rw" or mode == "readwrite" then
		flag = ser.__lib.SP_MODE_READ_WRITE
	else
		flag = ser.__lib.SP_MODE_READ_WRITE
	end

	local res = ser.__lib.sp_open( port.__port[0], flag )
	if res == ser.__lib.SP_OK then
		return port
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.close( port )

	assert( ffi.istype(ser.__port, port), "Port must be a valid port" )

	local res = ser.__lib.sp_close( port.__port[0] )
	if res == ser.__lib.SP_OK then
		return port
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.getName( port )

	assert( ffi.istype(ser.__port, port), "Port must be a valid port" )

	local name = ser.__lib.sp_get_port_name( port.__port[0] )
	if name ~= nil then
		return ffi.string( name )
	else
		return nil
	end
end

function ser.getDescription( port )

	assert( ffi.istype(ser.__port, port), "Port must be a valid port" )

	local desc = ser.__lib.sp_get_port_description( port.__port[0] )
	if desc ~= nil then
		return ffi.string( desc )
	else
		return nil
	end

end

function ser.getTransportType( port )

	assert( ffi.istype(ser.__port, port), "Port must be a valid port" )

	local typ = ser.__lib.sp_get_port_transport( port.__port[0] )
	if typ ~= nil then
		if typ == ser.__lib.SP_TRANSPORT_NATIVE then
			return "Native platform serial port"
		elseif typ == ser.__lib.SP_TRANSPORT_USB then
			return "USB serial port adapter"
		elseif typ == ser.__lib.SP_TRANSPORT_BLUETOOTH then
			return "Bluetooth serial port adapter"
		end
	else
		return nil
	end

end

function ser.getUSBBusAddress( port )

	assert( ffi.istype(ser.__port, port), "Port must be a valid port" )

	local usb_bus = ffi.new("int[1]")
	local usb_address = ffi.new("int[1]")

	local res = ser.__lib.sp_get_port_usb_bus_address( port.__port[0], usb_bus, usb_address )
	if res == ser.__lib.SP_OK then
		return tonumber(usb_bus[0]), tonumber(usb_address[0])
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.getUSBVIDPID( port )

	assert( ffi.istype(ser.__port, port), "Port must be a valid port" )

	local usb_vid = ffi.new("int[1]")
	local usb_pid = ffi.new("int[1]")

	local res = ser.__lib.sp_get_port_usb_vid_pid( port.__port[0], usb_vid, usb_pid )
	if res == ser.__lib.SP_OK then
		return tonumber(usb_vid[0]), tonumber(usb_pid[0])
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.getUSBManufacturer( port )

	assert( ffi.istype(ser.__port, port), "Port must be a valid port" )

	local manu = ser.__lib.sp_get_port_usb_manufacturer( port.__port[0] )
	if manu ~= nil then
		return ffi.string( manu )
	else
		return nil
	end

end

function ser.getUSBProduct( port )

	assert( ffi.istype(ser.__port, port), "Port must be a valid port" )

	local prod = ser.__lib.sp_get_port_usb_product( port.__port[0] )
	if prod ~= nil then
		return ffi.string( prod )
	else
		return nil
	end

end

function ser.getUSBSerial( port )

	assert( ffi.istype(ser.__port, port), "Port must be a valid port" )

	local serial = ser.__lib.sp_get_port_usb_serial( port.__port[0] )
	if serial ~= nil then
		return ffi.string( serial )
	else
		return nil
	end

end

function ser.getBluetoothAddress( port )

	assert( ffi.istype( ser.__port, port ), "Port must be a valid port" )

	local addr = ser.__lib.sp_get_port_bluetooth_address( port.__port[0] )
	if addr ~= nil then
		return ffi.string( addr )
	else
		return nil
	end

end

function ser.__getHandle( port, handle_ptr )

	assert( ffi.istype( ser.__port, port ), "Port must be a valid port" )

	local res = ser.__lib.sp_get_port_handle( port._port[0], handle_ptr )
	if res == ser.__lib.SP_OK then
		return handle_ptr
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.newConfig( )

	local config = ser.__config()

	local res = ser.__lib.sp_new_config( config.__config )
	if res == ser.__lib.SP_OK then
		return config
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.__freeConfig( config )

	assert( ffi.istype( ser.__config, config ), "Config must be a valid config" )

	ser.__lib.sp_free_config( port.__config[0] )

end

function ser.getConfig( port )

	assert( ffi.istype( ser.__port, port ), "Port must be a valid port" )

	local config = ser.newConfig()

	local res = ser.__lib.sp_get_config( port.__port[0], config.__config[0] )
	if res == ser.__lib.SP_OK then
		return config
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.setConfig( port, config )

	assert( ffi.istype( ser.__port, port ), "Port must be a valid port" )
	assert( ffi.istype( ser.__config, config ), "Config must be a valid config" )

	local res = ser.__lib.sp_set_config( port.__port[0], config.__config[0] )
	if res == ser.__lib.SP_OK then
		return port
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.setBaudrate( port, baud )

	assert( ffi.istype( ser.__port, port ), "Port must be a valid port" )
	assert( type( baud ) == "number", "Baudrate must be an integer" )

	local res = ser.__lib.sp_set_baudrate( port.__port[0], baud )
	if res == ser.__lib.SP_OK then
		return port
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.getConfigBaudrate( config )

	assert( ffi.istype( ser.__config, config ), "Config must be a valid config" )

	local baud = ffi.new("int[1]")

	local res = ser.__lib.sp_get_config_baudrate( config.__config[0], baud )
	if res == ser.__lib.SP_OK then
		return tonumber(baud[0])
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.setConfigBaudrate( config, baud )

	assert( ffi.istype( ser.__config, config ), "Config must be a valid config" )
	assert( type( baud ) == "number", "Baudrate must be an integer" )

	local res = ser.__lib.sp_set_config_baudrate( config.__config[0], baud )
	if res == ser.__lib.SP_OK then
		return config
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.setBits( port, bits )

	assert( ffi.istype( ser.__port, port ), "Port must be a valid port" )
	assert( type( bits ) == "number", "Baudrate must be an integer" )

	local res = ser.__lib.sp_set_bits( port.__port[0], bits )
	if res == ser.__lib.SP_OK then
		return port
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.getConfigBits( config )

	assert( ffi.istype( ser.__config, config ), "Config must be a valid config" )
	
	local bits = ffi.new("int[1]")

	local res = ser.__lib.sp_get_config_bits( config.__config[0], bits )
	if res == ser.__lib.SP_OK then
		return tonumber(bits[0])
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.setConfigBits( config, bits )

	assert( ffi.istype( ser.__config, config ), "Config must be a valid config" )
	assert( type( bits ) == "number", "Baudrate must be an integer" )

	local res = ser.__lib.sp_set_config_bits( config.__config[0], bits )
	if res == ser.__lib.SP_OK then
		return config
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.setParity( port, parity )
	local lib = ser.__lib
	assert( ffi.istype( ser.__port, port ), "Port must be a valid port" )

	local par;
	if parity == "invalid" then
		par = lib.SP_PARITY_INVALID
	elseif parity == "none" then
		par = lib.SP_PARITY_NONE
	elseif parity == "odd" then
		par = lib.SP_PARITY_ODD
	elseif parity == "even" then
		par = lib.SP_PARITY_EVEN
	elseif parity == "mark" then
		par = lib.SP_PARITY_MARK
	elseif parity == "space" then
		par = lib.SP_PARITY_SPACE
	else
		par = lib.SP_PARITY_INVALID
	end

	local res = lib.sp_set_baudrate( port.__port[0], par )
	if res == lib.SP_OK then
		return port
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.getConfigParity( config )

	local lib = ser.__lib
	assert( ffi.istype( ser.__config, config ), "Config must be a valid config" )
	
	local par = ffi.new("int[1]")

	local res = ser.__lib.sp_get_config_bits( config.__config[0], par )
	if res == ser.__lib.SP_OK then
		
		local parity = tonumber(par[0])

		if parity == lib.SP_PARITY_INVALID then
			return "invalid"
		elseif parity == lib.SP_PARITY_NONE then
			return "none"
		elseif parity == lib.SP_PARITY_ODD then
			return "odd"
		elseif parity == lib.SP_PARITY_EVEN then
			return "event"
		elseif parity == lib.SP_PARITY_MARK then
			return "mark"
		elseif parity == lib.SP_PARITY_SPACE then
			return "space"
		end

	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.setConfigParity( config, parity )
	local lib = ser.__lib

	assert( ffi.istype( ser.__config, config ), "Config must be a valid config" )

	local par;
	if parity == "invalid" then
		par = lib.SP_PARITY_INVALID
	elseif parity == "none" then
		par = lib.SP_PARITY_NONE
	elseif parity == "odd" then
		par = lib.SP_PARITY_ODD
	elseif parity == "even" then
		par = lib.SP_PARITY_EVEN
	elseif parity == "mark" then
		par = lib.SP_PARITY_MARK
	elseif parity == "space" then
		par = lib.SP_PARITY_SPACE
	else
		par = lib.SP_PARITY_INVALID
	end

	local res = ser.__lib.sp_set_config_parity( config.__config[0], par )
	if res == ser.__lib.SP_OK then
		return config
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.setStopbits( port, stopbits )

	assert( ffi.istype( ser.__port, port ), "Port must be a valid port" )
	assert( type( stopbits ) == "number", "Baudrate must be an integer" )

	local res = ser.__lib.sp_set_stopbits( port.__port[0], stopbits )
	if res == ser.__lib.SP_OK then
		return port
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.getConfigStopbits( config )

	assert( ffi.istype( ser.__config, config ), "Config must be a valid config" )
	
	local bits = ffi.new("int[1]")

	local res = ser.__lib.sp_get_config_stopbits( config.__config[0], bits )
	if res == ser.__lib.SP_OK then
		return tonumber(bits[0])
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.setConfigStopbits( config, bits )

	assert( ffi.istype( ser.__config, config ), "Config must be a valid config" )
	assert( type( bits ) == "number", "Baudrate must be an integer" )

	local res = ser.__lib.sp_set_config_stopbits( config.__config[0], bits )
	if res == ser.__lib.SP_OK then
		return config
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.setRTS( port, rts )

	local lib = ser.__lib
	assert( ffi.istype( ser.__port, port ), "Port must be a valid port" )
	
	local rt;
	if rts == "invalid" then
		rt = lib.SP_RTS_INVALID
	elseif rts == "off" then
		rt = lib.SP_RTS_OFF
	elseif rts == "on" then
		rt = lib.SP_RTS_ON
	elseif rts == "flow_control" then
		rt = lib.SP_RTS_FLOW_CONTROL
	else
		rt = lib.SP_RTS_INVALID
	end

	local res = lib.sp_set_rts( port.__port[0], rts )
	if res == lib.SP_OK then
		return port
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.getConfigRTS( config )

	local lib = ser.__lib
	assert( ffi.istype( ser.__config, config ), "Config must be a valid config" )
	
	local rt = ffi.new("int[1]")

	local res = ser.__lib.sp_get_config_rts( config.__config[0], rt )
	if res == ser.__lib.SP_OK then

		local rts = tonumber(rts[0])

		if rt == lib.SP_RTS_INVALID then
			return "invalid"
		elseif rts == lib.SP_RTS_OFF then
			return "off"
		elseif rts == lib.SP_RTS_ON then
			return "on"
		elseif rts == lib.SP_RTS_FLOW_CONTROL then
			return "flow_control"
		end

	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.setConfigRTS( config, rts )

	local lib = ser.__lib
	assert( ffi.istype( ser.__config, config ), "Config must be a valid config" )
	
	local rt;
	if rts == "invalid" then
		rt = lib.SP_RTS_INVALID
	elseif rts == "off" then
		rt = lib.SP_RTS_OFF
	elseif rts == "on" then
		rt = lib.SP_RTS_ON
	elseif rts == "flow_control" then
		rt = lib.SP_RTS_FLOW_CONTROL
	else
		rt = lib.SP_RTS_INVALID
	end

	local res = ser.__lib.sp_set_config_rts( config.__config[0], rt )
	if res == ser.__lib.SP_OK then
		return config
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.setCTS( port, cts )

	local lib = ser.__libs
	assert( ffi.istype( ser.__port, port ), "Port must be a valid port" )
	
	local ct;
	if cts == "invalid" then
		ct = lib.SP_CTS_INVALID
	elseif cts == "ignore" then
		ct = lib.SP_CTS_IGNORE
	elseif cts == "flow_control" then
		ct = lib.SP_CTS_FLOW_CONTROL
	else 
		ct = lib.SP_CTS_INVALID
	end  

	local res = ser.__lib.sp_set_cts( port.__port[0], ct )
	if res == ser.__lib.SP_OK then
		return port
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.getConfigCTS( config )

	local lib = ser.__lib
	assert( ffi.istype( ser.__config, config ), "Config must be a valid config" )

	local ct = ffi.new("int[1]")

	local res = ser.__lib.sp_get_config_cts( config.__config[0], ct )
	if res == ser.__lib.SP_OK then

		local cts = tonumber(ct[0])

		if cts == lib.SP_CTS_INVALID then
			return "invalid"
		elseif cts == lib.SP_CTS_IGNORE then
			return "ignore"
		elseif cts == lib.SP_CTS_FLOW_CONTROL then
			return "flow_control"
		end  
		
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.setConfigCTS( config, cts)

	local lib = ser.__lib
	assert( ffi.istype( ser.__config, config ), "Config must be a valid config" )
	
	local ct;
	if cts == "invalid" then
		ct = lib.SP_CTS_INVALID
	elseif cts == "ignore" then
		ct = lib.SP_CTS_IGNORE
	elseif cts == "flow_control" then
		ct = lib.SP_CTS_FLOW_CONTROL
	else 
		ct = lib.SP_CTS_INVALID
	end  

	local res = ser.__lib.sp_set_config_cts( config.__config[0], ct )
	if res == ser.__lib.SP_OK then
		return config
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.setDTR( port, dtr )

	local lib = ser.__libs
	assert( ffi.istype( ser.__port, port ), "Port must be a valid port" )
	
	local dt;
	if dtr == "invalid" then
		dt = lib.SP_DTR_INVALID
	elseif dtr == "off" then
		dt = lib.SP_DTR_OFF
	elseif dtr == "on" then
		dt = lib.SP_DTR_ON
	elseif dtr == "flow_control" then
		dt = lib.SP_DTR_FLOW_CONTROL
	else 
		dt = lib.SP_DTR_INVALID
	end  

	local res = ser.__lib.sp_set_dtr( port.__port[0], dt )
	if res == ser.__lib.SP_OK then
		return port
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.getConfigDTR( config )

	local lib = ser.__lib
	assert( ffi.istype( ser.__config, config ), "Config must be a valid config" )

	local dt = ffi.new("int[1]")

	local res = ser.__lib.sp_get_config_dtr( config.__config[0], dt )
	if res == ser.__lib.SP_OK then

		local dtr = tonumber(dt[0])

		if dtr == lib.SP_DTR_INVALID then
			return "invalid"
		elseif dtr == lib.SP_DTR_OFF then
			return "off" 
		elseif dtr == lib.SP_DTR_ON then
			return "on"
		elseif dtr == lib.SP_DTR_FLOW_CONTROL then
			return "flow_control"
		end  
		
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.setConfigDTR( config, dtr )

	local lib = ser.__lib
	assert( ffi.istype( ser.__config, config ), "Config must be a valid config" )
	
	local dt;
	if dtr == "invalid" then
		dt = lib.SP_DTR_INVALID
	elseif dtr == "off" then
		dt = lib.SP_DTR_OFF
	elseif dtr == "on" then
		dt = lib.SP_DTR_ON
	elseif dtr == "flow_control" then
		dt = lib.SP_DTR_FLOW_CONTROL
	else 
		dt = lib.SP_DTR_INVALID
	end  

	local res = ser.__lib.sp_set_config_dtr( config.__config[0], dt )
	if res == ser.__lib.SP_OK then
		return config
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.setDSR( port, dsr )

	local lib = ser.__libs
	assert( ffi.istype( ser.__port, port ), "Port must be a valid port" )
	
	local ds;
	if dsr == "invalid" then
		ds = lib.SP_DSR_INVALID
	elseif dsr == "ignore" then
		ds = lib.SP_DSR_IGNORE
	elseif dsr == "flow_control" then
		ds = lib.SP_DSR_FLOW_CONTROL
	else 
		ds = lib.SP_DSR_INVALID
	end  

	local res = ser.__lib.sp_set_dsr( port.__port[0], ds )
	if res == ser.__lib.SP_OK then
		return port
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.getConfigDSR( config )

	local lib = ser.__lib
	assert( ffi.istype( ser.__config, config ), "Config must be a valid config" )

	local ds = ffi.new("int[1]")

	local res = ser.__lib.sp_get_config_dsr( config.__config[0], ds )
	if res == ser.__lib.SP_OK then

		local dsr = tonumber(ds[0])

		if dsr == lib.SP_DSR_INVALID then
			return "invalid"
		elseif dsr == lib.SP_DSR_IGNORE then
			return "ignore"
		elseif dsr == lib.SP_DSR_FLOW_CONTROL then
			return "flow_control"
		end 
		
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.setConfigDSR( config, dsr )

	local lib = ser.__lib
	assert( ffi.istype( ser.__config, config ), "Config must be a valid config" )
	
	local ds;
	if dsr == "invalid" then
		ds = lib.SP_DSR_INVALID
	elseif dsr == "ignore" then
		ds = lib.SP_DSR_IGNORE
	elseif dsr == "flow_control" then
		ds = lib.SP_DSR_FLOW_CONTROL
	else 
		ds = lib.SP_DSR_INVALID
	end  

	local res = ser.__lib.sp_set_config_dsr( config.__config[0], ds )
	if res == ser.__lib.SP_OK then
		return config
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.setXONOFF( port, xonoff )

	local lib = ser.__libs
	assert( ffi.istype( ser.__port, port ), "Port must be a valid port" )
	
	local x;
	if xonoff == "invalid" then
		x = lib.SP_XONXOFF_INVALID
	elseif xonoff == "disabled" then
		x = lib.SP_XONXOFF_DISABLED
	elseif xonoff == "in" then
		x = lib.SP_XONXOFF_IN
	elseif xonoff == "out" then
		x = lib.SP_XONXOFF_OUT
	elseif xonoff == "inout" then
		x = lib.SP_XONXOFF_INOUT
	else 
		x = lib.SP_XONXOFF_INVALID
	end  

	local res = ser.__lib.sp_set_xon_xoff( port.__port[0], x )
	if res == ser.__lib.SP_OK then
		return port
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.getConfigXONOFF( config )

	local lib = ser.__lib
	assert( ffi.istype( ser.__config, config ), "Config must be a valid config" )

	local x = ffi.new("int[1]")

	local res = ser.__lib.sp_get_config_xon_xoff( config.__config[0], x )
	if res == ser.__lib.SP_OK then

		local xonoff = tonumber(x[0])

		if xonoff == lib.SP_XONXOFF_INVALID then
			return "invalid"
		elseif xonoff == lib.SP_XONXOFF_DISABLED then
			return "disabled"
		elseif xonoff == lib.SP_XONXOFF_IN then
			return "in"
		elseif xonoff == lib.SP_XONXOFF_OUT then
			return "out"
		elseif xonoff == lib.SP_XONXOFF_INOUT then
			return "inout"
		end 
		
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.setConfigXONOFF( config, xonoff )

	local lib = ser.__lib
	assert( ffi.istype( ser.__config, config ), "Config must be a valid config" )
	
	local x;
	if xonoff == "invalid" then
		x = lib.SP_XONXOFF_INVALID
	elseif xonoff == "disabled" then
		x = lib.SP_XONXOFF_DISABLED
	elseif xonoff == "in" then
		x = lib.SP_XONXOFF_IN
	elseif xonoff == "out" then
		x = lib.SP_XONXOFF_OUT
	elseif xonoff == "inout" then
		x = lib.SP_XONXOFF_INOUT
	else 
		x = lib.SP_XONXOFF_INVALID
	end  

	local res = ser.__lib.sp_set_config_xon_xoff( config.__config[0], x )
	if res == ser.__lib.SP_OK then
		return config
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.setFlowcontrol( port, flow )

	local lib = ser.__libs
	assert( ffi.istype( ser.__port, port ), "Port must be a valid port" )
	
	local flo;
	if dsr == "none" then
		flo = lib.SP_FLOWCONTROL_NONE
	elseif dsr == "xonxoff" then
		flo = lib.SP_FLOWCONTROL_XONXOFF
	elseif dsr == "rtscts" then
		flo = lib.SP_FLOWCONTROL_RTSCTS
	elseif dsr == "dtrdsr" then
		flo = lib.SP_FLOWCONTROL_DTRDSR
	else 
		flo = lib.SP_FLOWCONTROL_NONE
	end  

	local res = ser.__lib.sp_set_flowcontrol( port.__port[0], flo )
	if res == ser.__lib.SP_OK then
		return port
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.setConfigFlowcontrol( config, flow )

	local lib = ser.__lib
	assert( ffi.istype( ser.__config, config ), "Config must be a valid config" )
	
	local flo;
	if dsr == "none" then
		flo = lib.SP_FLOWCONTROL_NONE
	elseif dsr == "xonxoff" then
		flo = lib.SP_FLOWCONTROL_XONXOFF
	elseif dsr == "rtscts" then
		flo = lib.SP_FLOWCONTROL_RTSCTS
	elseif dsr == "dtrdsr" then
		flo = lib.SP_FLOWCONTROL_DTRDSR
	else 
		flo = lib.SP_FLOWCONTROL_NONE
	end 

	local res = ser.__lib.sp_set_config_flowcontrol( config.__config[0], flo )
	if res == ser.__lib.SP_OK then
		return config
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.read( port, buffsize, timeout ) -- if a timeout is given, uses blocking else unblocking read

	if timeout then
		return ser.blockingRead( port, buffsize, timeout )
	else
		return ser.nonblockingRead( port, buffsize )
	end

end

function ser.blockingRead( port, buffsize, timeout )

	assert( ffi.istype(ser.__port, port), "Port must be a valid port" )

	local bsize = buffsize or port:inputWaiting()
	if not bsize or bsize == 0 then
		return nil
	end

	local buf = ser.__uint8( bsize )
	local res = ser.__lib.sp_blocking_read( port.__port[0], buf, bsize, timeout )
	
	if res == 0 then return nil, "timeout" end

	if res > 0 then
		return ffi.string( buf , res )
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.blockingReadNext( port, buffsize, timeout )

	assert( ffi.istype(ser.__port, port), "Port must be a valid port" )

	local bsize = buffsize or port:inputWaiting()
	if not bsize or bsize == 0 then
		return nil
	end

	local buf = ser.__uint8( bsize )
	local res = ser.__lib.sp_blocking_read_next( port.__port[0], buf, bsize, timeout )
	
	if res == 0 then return nil, "timeout" end

	if res > 0 then
		return ffi.string( buf , res )
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.nonblockingRead( port, buffsize )

	assert( ffi.istype(ser.__port, port), "Port must be a valid port" )

	local bsize = buffsize or port:inputWaiting()
	if not bsize or bsize == 0 then
		return nil
	end

	local buf = ser.__uint8( bsize )
	local res = ser.__lib.sp_nonblocking_read( port.__port[0], buf, bsize )
	
	if res == 0 then return nil end

	if res > 0 then
		return ffi.string( buf , res )
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.write( port, data, timeout ) -- if a timeout is given, uses blocking else unblocking write
	if data == "" then return nil, "no data" end
	if timeout then
		return ser.blockingWrite( port, data, timeout )
	else
		return ser.nonblockingWrite( port, data )
	end
end

function ser.blockingWrite( port, data, timeout )

	assert( ffi.istype(ser.__port, port), "Port must be a valid port" )

	local res = ser.__lib.sp_blocking_write( port.__port[0], data, #data, timeout )

	if res >= 0 then
		return tonumber(res)
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.nonblockingWrite( port, data )

	assert( ffi.istype(ser.__port, port), "Port must be a valid port" )

	print(#data)

	local res = ser.__lib.sp_nonblocking_write( port.__port[0], data, #data )

	if res >= 0 then
		return tonumber(res)
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.inputWaiting( port )

	assert( ffi.istype(ser.__port, port), "Port must be a valid port" )

	local res = ser.__lib.sp_input_waiting( port.__port[0] )

	if res >= 0 then
		return res
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.outputWaiting( port )

	assert( ffi.istype(ser.__port, port), "Port must be a valid port" )

	local res = ser.__lib.sp_output_waiting( port.__port[0] )

	if res >= 0 then
		return res
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.flush( port, which )

	local lib = ser.__lib
	assert( ffi.istype(ser.__port, port), "Port must be a valid port" )

	local w;
	if which == "input" then
		w = lib.SP_BUF_INPUT
	elseif which == "output" then
		w = lib.SP_BUF_OUTPUT
	elseif which == "both" then
		w = lib.SP_BUF_BOTH
	else
		w = lib.SP_BUF_BOTH
	end 

	local res = ser.__lib.sp_flush( port.__port[0], w )

	if res == lib.SP_OK then
		return port
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.drain( port )

	assert( ffi.istype(ser.__port, port), "Port must be a valid port" )

	local res = ser.__lib.sp_drain( port.__port[0] )

	if res == ser.__lib.SP_OK then
		return port
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.newEventSet()

	local events = ser.__events()

	local res = ser.__lib.sp_new_event_set( events.__events )
	if res == ser.__lib.SP_OK then
		return events
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.addPortEvents( events, port, mask )

	local lib = ser.__lib
	assert( ffi.istype(ser.__events, events), "Events must be a valid event set" )
	assert( ffi.istype(ser.__port, port), "Port must be a valid port" )

	local m;
	if type(mask) == "table" then
		m = 0
		for _,v in pairs(mask) do
			if v == "rx_ready" then
				bit.band(m, lib.SP_EVENT_RX_READY )
			elseif v == "tx_ready" then
				bit.band(m, lib.SP_EVENT_TX_READY )
			elseif v == "error" then
				bit.band(m, lib.SP_EVENT_ERROR )
			end
		end
	elseif mask == "rx_ready" then
		m = lib.SP_EVENT_RX_READY
	elseif mask == "tx_ready" then
		m = lib.SP_EVENT_TX_READY
	elseif mask == "error" then
		m = lib.SP_EVENT_ERROR
	else
		return nil, "Invalid mask"
	end

	local res = lib.sp_add_port_events( events.__events[0], port.__port[0], m )
	if res == lib.SP_OK then
		return events
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.wait( events, timeout )

	assert( ffi.istype(ser.__events, events), "Events must be a valid event set" )

	local res = ser.__lib.sp_wait( events.__events[0], timeout )
	if res == ser.__lib.SP_OK then
		return events
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.__freeEventSet( events )

	assert( ffi.istype(ser.__events, events), "Events must be a valid event set" )

	ser.__lib.sp_free_event_set( events.__events[0] )

end

function ser.getSignals( port )

	assert( ffi.istype(ser.__port, port), "Port must be a valid port" )

	local mask = ffi.new("int[1]")

	local res = ser.__lib.sp_get_signals( port.__port[0], mask )
	if res == ser.__lib.SP_OK then

		local m = mask[0]
		local signals = {
			cts = bit.bor(m, 1) == m,
			dsr = bit.bor(m, 2) == m,
			dcd = bit.bor(m, 4) == m,
			ri = bit.bor(m, 8) == m
		}
		return signals
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.startBreak( port )

	assert( ffi.istype(ser.__port, port), "Port must be a valid port" )

	local res = ser.__lib.sp_start_break( port.__port[0] )
	if res == ser.__lib.SP_OK then
		return port
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.endBreak( port )

	assert( ffi.istype(ser.__port, port), "Port must be a valid port" )

	local res = ser.__lib.sp_end_break( port.__port[0] )
	if res == ser.__lib.SP_OK then
		return port
	else
		return ser.__getLastErrorMesage( res )
	end

end

function ser.__getLastErrorMesage( errCode )
	local lib = ser.__lib

	if errCode == lib.SP_ERR_ARG then
		return false, "Invalid arguments"
	elseif errCode == lib.SP_ERR_FAIL then
		local message = lib.sp_last_error_message()
		local str = ffi.string(message)
		lib.sp_free_error_message(message)
		return false, str
	elseif errCode == lib.SP_ERR_MEM then
		return false, "A memory allocation failed while executing the operation"
	elseif errCode == lib.SP_ERR_SUPP then
		return false, "The requested operation is not supported by this system or device"
	else 
		return false, "Unknown error code. This shouldn't happen..."
	end

end

function ser.getMajorPackageVersion()

	return tonumber(ser.__lib.sp_get_major_package_version())

end

function ser.getMinorPackageVersion()

	return tonumber(ser.__lib.sp_get_minor_package_version())

end

function ser.getMicroPackageVersion()

	return tonumber(ser.__lib.sp_get_micro_package_version())

end

function ser.getPackageVersion()

	return ffi.string(ser.__lib.sp_get_package_version_string())

end

function ser.getCurrentLibVersion()

	return tonumber(ser.__lib.sp_current_lib_version())

end

function ser.getRevisionLibVersion()

	return tonumber(ser.__lib.sp_revision_lib_version())

end

function ser.getAgeLibVersion()

	return tonumber(ser.__lib.sp_age_lib_version())

end

function ser.getLibVersion()

	return ffi.string(ser.__lib.sp_get_lib_version_string())

end

ser.structs = {}
ser.pointers = {}
ser.packets = {}

function ser.createStruct( name, fields, map)
	-- option to add states to structs, if the first byte does not match the state, it is returned directly instead of the struct

	local struct = string.format("typedef struct { %s } %s;", fields, name)

	ffi.cdef(struct)

	ser.structs[name] = ffi.typeof(name)
	ser.pointers[name] = ffi.typeof(name.."*")

	if map then
        map.name = name
        table.insert(ser.packets, map)
        ser.packets[name] = #ser.packets
    end
end 

function ser.setStruct( name, data )
	return ffi.new( ser.structs[ name ], data )
end

function ser.encodeStruct( data )
	return ffi.string( ffi.cast( "const char*", data ), ffi.sizeof( data ) )
end

function ser.decodeStruct( name, data )
	return ffi.cast( ser.pointers[ name ], data ) [ 0 ]
end

function ser.readStruct( port, name, timeout, checkNum, timeout2 )

	local size = ffi.sizeof[ name ]

	if checkNum then

		local data, err = ser.blockingRead( port, 1, timeout )
		if data then
			if tostring(data):byte() == tonumber(checkNum) then

				local data2, err2 = ser.blockingRead( port, size - 1, timeout2 or timeout )

				if data2 then
					return ser.decodeStruct( name, data .. data2 )
				else
					return ser.decodeStruct( name, data )
				end

			else
				return data
			end
		else
			return data, err
		end

	else
		local data, err = ser.blockingRead( port, size, timeout )
		if data then
			return ser.decodeStruct( name, data )
		else
			return data, err
		end
	end

end

ser.__port_meta = {
	__tostring = function( port )
		return  "Serialport: " .. tostring( ser.getName( port ) )
	end,
	__gc = ser.__freePort,
	__index = {
		__freePort = ser.__freePort,
		copy = ser.copyPort,
		open = ser.open,
		close = ser.close,
		getName = ser.getName,
		getDescription = ser.getDescription,
		getTransportType = ser.getTransportType,
		getUSBBusAddress = ser.getUSBBusAddress,
		getUSBVIDPID = ser.getUSBVIDPID,
		getUSBManufacturer = ser.getUSBManufacturer,
		getUSBProduct = ser.getUSBProduct,
		getUSBSerial = ser.getUSBSerial,
		getBluetoothAddress = ser.getBluetoothAddress,
		__getHandle = ser.__getHandle,
		getConfig = ser.getConfig,
		setConfig = ser.setConfig,
		setBaudrate = ser.setBaudrate,
		setBits = ser.setBits,
		setParity = ser.setParity,
		setStopbits = ser.setStopbits,
		setRTS = ser.setRTS,
		setCTS = ser.setCTS,
		setDTR = ser.setDTR,
		setDSR = ser.setDSR,
		setXONOFF = ser.setXONOFF,
		setFlowcontrol = ser.setFlowcontrol,
		read = ser.read,
		blockingRead = ser.blockingRead,
		blockingReadNext = ser.blockingReadNext,
		nonblockingRead = ser.nonblockingRead,
		write = ser.write,
		blockingWrite = ser.blockingWrite,
		nonblockingWrite = ser.nonblockingWrite,
		inputWaiting = ser.inputWaiting,
		outputWaiting = ser.outputWaiting,
		flush = ser.flush,
		drain = ser.drain,
		getSignals = ser.getSignals,
		startBreak = ser.startBreak,
		endBreak = ser.endBreak,

	}
}

ser.__ports_meta = {
	__tostring = function( ports )
		return "Serialport list: " .. tostring( ports:getPortCount() ) .. " Port(s)"
	end,
	__gc = ser.__freePortList,
	__index = {
		getPortCount = ser.getListPortCount,
		getNames = ser.getListNames,
		getDescriptions = ser.getListDescriptions,
		getTransportTypes = ser.getListTransportTypes,
		getPort = ser.getPortFromList,
		__freePortList = ser.__freePortList
	},
	__newindex = function() end 
}

ser.__config_meta = {
	__tostring = function( config )
		return "Serialport Config"
	end,
	__gc = ser.__freeConfig,
	__index = {
		__freeConfig = ser.__freeConfig,
		getBaudrate = ser.getConfigBaudrate,
		setBaudrate = ser.setConfigBaudrate,
		getBits = ser.getConfigBits,
		setBits = ser.setConfigBits,
		getParity = ser.getConfigParity,
		setParity = ser.setConfigParity,
		getStopbits = ser.getConfigStopbits,
		setStopbits = ser.setConfigStopbits,
		getRTS = ser.getConfigRTS,
		setRTS = ser.setConfigRTS,
		getCTS = ser.getConfigCTS,
		setCTS = ser.setConfigCTS,
		getDTR = ser.getConfigDTR,
		setDTR = ser.setConfigDTR,
		getDSR = ser.getConfigDSR,
		setDSR = ser.setConfigDSR,
		getXONOFF = ser.getConfigXONOFF,
		setXONOFF = ser.setConfigXONOFF,
		setFlowcontrol = ser.setConfigFlowcontrol,
	}
}

ser.__config_events = {
	__tostring = function( ports )
		return "Serialport event set"
	end,
	__gc = ser.__freeEventSet,
	__index = {
		wait = ser.wait,
		addPortEvents = ser.addPortEvents
	},
	__newindex = function() end 
}

return ser